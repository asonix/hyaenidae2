# Hyaenidae 2

- Discussion on [matrix](https://matrix.to/#/#hyaenidae2:conduit.asonix.dog)

Hyaenidae 2 is my second attempt at a federated Art Gallery site in Rust. The first attempt was in
February of 2021, and it burned me out really fast. This second attempt is an effort to continue the
ideas of the first with the help of other programmers besides myself.

See the efforts of the first attempt: https://git.asonix.dog/asonix/hyaenidae

Hyaenidae 2 is not yet a concrete repository, but more a collection of ideas for how a federated art
gallery site should work. The following is a list of technologies I'm hoping to leverage in
Hyaenidae 2:
- Data Storage:
  - [BonsaiDB](https://github.com/khonsulabs/bonsaidb/)

    I have minimal experience with BonsaiDB, however, it seems like a good way to go for a native
    rust database that can be run either in-process or as a separate server. Relying on BonsaiDB to
    store not only Activities, but also a Job Queue seems to make sense. With support for table-like
    data as well as atomic key/value pairs, it supports cases that Postgres or Redis might also suit
    in more traditional applications.

- Web Server
  - [Actix Web](https://github.com/actix/actix-web)

    I have significant experience building with Actix Web. It's a mature framework that has support
    for everything needed to build API servers for PWAs, or for rendering static pages.

  - [Axum](https://github.com/tokio-rs/axum/)

    I have no experience with Axum, but it seems to be very similar to Actix Web in many ways. The
    primary distinction is that Axum relies on Hyper and Tokio's multi-threaded executor, while
    Actix Web uses multiple single-threaded tokio executors and has a partially-custom HTTP
    implementation.

    The primary benefit of going with Axum over Actix Web is it has support from
    [Dioxus](https://github.com/dioxuslabs/dioxus) for running a live-view application. Live View is
    a pattern that originated in Elixir with Phoenix LiveView that allows for a web application's
    frontend code to be written in the same language as the backend, and run on the server. A simple
    JavaScript application runs on the frontend and receives updates about the application's view
    from the server over websockets, rendering the diff to the page. Interaction from the frontend
    is forwarded to the server to perform any logic on application state.

    Whether we want to pursue a Live View application will be the primary factor for using Axum.
    Otherwise it will be the personal preference of the contributors.

- Frontend
  - [Dioxus](https://github.com/dioxuslabs/dioxus)

    While I've already explained Dioxus Live View, Dioxus can also be used to render static views
    easily from any backend framework. This is a compelling option for static rendering.

  - [Ructe](https://github.com/kaj/ructe)

    Ructe is another compelling static-rendered templating engine, and one I have experience with.
    With the release of Dioxus 0.3, I'm less tempted to build new applications with Ructe.

  - [Elm](elm-lang.org)

    Elm is a purely functional programming language that compiles to JavaScript for use in Browser
    Applications. It can handle Single Page Applications, but can also be used in the "island"
    format that is becoming popular currently. In fact, the Island approach is the one NoRedInk, the
    employers of the Elm BDFL, has taken since introducing Elm. For this reason, SPAs in Elm seem to
    have more boilerplate than you might want, but it's a simple language and nothing about SPAs in
    Elm is overly complex.

  - [Your Favorite JavaScript/TypeScript Framework]()

    If we end up going with an off-the-shelf JavaScript option like React, Vue, Solid, or Svelt, I'm
    unlikely to want to contribute to the frontend code. I do some Frontend work in JavaScript and
    TypeScript for work and I don't want to bring those technologies to my personal projects.


## Server Features

### Approval-based federation

- Servers need to request federation with each other using the same sort of mechanism as Mastodon with Relays
- Server Actors are the Most Trusted actors in the network, and all other actors derive authority from the server actor
- Server Actor discovery must be standardized
  - Maybe in a new .well-known endpoint
  - Maybe webfinger with a custom prefix 
    - resource=server:domain.tld might work
- Non-federated servers must not be able to query user Activities or Actors
- Anonymous requests must not be able to query user Activities or Actors


### JSON-LD

- incoming activities are expanded and framed, JSON-LD signatures are always verified
- outgoing activities are all signed
- Serving activities from fetch endpoints signs on request 
  - activities are not stored with their signatures

### HTTP Signatures

- not required for post distribution
- required for post fetching
  - Requests and Responses must be signed by the appropriate actors


### API

- In order to support 3rd party clients (I like these) and cross-posters (artists like these), We should support API access via OAuth2.
- The API should have access to all server functionality. It might even be used by our own frontend, depending on how that's built.


### Moderation

- Moderation Actors can be created by the server
  - Attached to existing local user accounts
- Moderation Actors have permission to interact with activities they did not create from the server on which they reside
  - edit (make modifications to)
    - title
    - text content
    - tags
  - delete
    - attachments from activity
    - whole activities
  - lock (prevent further edits)
- Moderation Actors have permission to interact with activities they did not create from foreign servers
  - hide
    - whole activities
      - posts
      - tags
      - profiles
      - etc
- Moderation Actors have permission to Modify server federation
  - Accept new federation requests
  - Reject new federation requests
  - Create new federation requests
  - Defederate from federated servers
- Moderation Actors have Reports queue


### User Actors

- Represented by machine-assigned IDs (UUIDs?)
- Aliased by user-assigned names
  - Tagging a user (e.g. @user@domain.tld) will reference the machine-id in the activity. This allows users to change their handles without breaking federation or tags



## Site Features

### Actor Interactions

- Actors can Follow other Actors
  - Follows are always Requested
    - Actors can configure automatic accepting of requests
  - Follows lead to all posts from followed users showing in the followers' feeds
- Actors can Mute other Actors
  - Posts from muted actors will not show in muter's feeds
- Actors can Block other Actors
  - Posts from blocked actors will not show in blocker's feeds
  - Posts from blocked actors mentioning blocker will be rejected
  - Replies from blocked actor will not have their contents or author visible to blocker
  - Blocked Actor will not be able to visit Blocker's profile
  - Blocked Actor will not see posts from Blocker
  - Blocked Actor will not see contents or author of replies from Blocker
- Actors can Mention other Actors
  - Mentioned Actors will receive a notification that they have been mentioned

### Actor Profiles

- Creation
  - Must have unique Handle for the server
  - Must have Display Name
  - Can add Banner Image
  - Can add Profile Image
  - Can add Profile Description
  - Can add Profile Fields
    - Key/Value pairs
  - Can add Profile Tags
    - not inline (no hashtags)
- Modification
  - By Original Actor
    - Can change Handle
    - Can change Display Name
    - Can change Banner Image
    - Can change Pofile Image
    - Can change Profile Description
    - Can change Profile Fields
    - Can Add/Remove profile tags
  - By Moderation Actor
    - Can hide entire profile
    - Can lock entire profile (prevent further edits from Original Actor)
- Report
  - By other User Actor
    - Can report entire profile
- Deletion
  - By Original Actor
    - Can delete entire profile
  - By Moderation Actor
    - Can delete entire profile


### Image Posts

- Creation
  - Can have different privacy levels
    - Public (Visible in public feeds)
    - Unlisted (Visible on post page, but not in public feeds)
    - Followers-Only (Visible in followers' feeds)
    - Direct (visible only to mentioned actors)
  - Multiple Images can be attached to a single post
    - Limit of 10
    - All image thumbnails appear on image post page
    - only one image is previewed at a time
    - Each image can have a unique description (alt-text)
  - Title is required
  - Description is optional
  - Can have attached tags
    - not inline (no hashtags)
  - Can have attached mentions
    - inline
  - Can have 'sensitive' designation
  - Can select Folders to add to post
- Modification
  - By Original Actor
    - Add/Remove tags
    - Change post title
    - Change post description
    - Add/Remove images
    - Edit image descriptions
    - Can Add/Remove 'sensitive' designation
    - Can Add/Remove post Folders
  - By Server Moderator
    - Add/Remove tags
    - Change post title
    - Change post description
    - Remove images
    - Edit image descriptions
    - Can Add/Remove 'sensitive' designation
    - Can Hide (prevent post from showing in feeds)
    - Can Lock (prevent futher edits by Original Actor)
      - Further replies to Locked posts will be rejected
  - By other User Actors
    - Favorite (add to Favorites Collection)
    - Boost (Announce to followers)
    - Add to Collection
    - Request Add/Remove tags
      - Original Actor or Moderator Actor must approve
- Report
  - By Original Actor
    - Report requested tag changes
  - By other User Actors
    - Report specific Tags
    - Report specific Image
    - Report whole post
- Deletion
  - By Original Actor
    - Delete whole post
  - By Moderator Actor
    - Delete whole post


### Text Posts

- Creation
  - Can have different privacy levels
    - Unlisted (Visible on post page, and in followers' feeds)
    - Followers-Only (Visible in followers' feeds)
    - Direct (visible only to mentioned actors)
  - Title is optional
  - Description is required
  - Can have attached tags
    - not inline (no hashtags)
  - Can have attached mentions
    - inline
  - Can have 'sensitive' designation
- Modification
  - By Original Actor
    - Add/Remove tags
    - Change post title
    - Change post description
    - Can Add/Remove 'sensitive' designation
  - By Moderator Actor
    - Add/Remove tags
    - Change post title
    - Change post description
    - Can Add/Remove 'sensitive' designation
    - Can Hide (prevent post from showing in feeds)
    - Can Lock (prevent futher edits by Original Actor)
      - Further replies to Locked posts will be rejected
- Report
  - By Original Actor
    - Report requested tag changes
  - By other User Actors
    - Report specific Tags
    - Report whole post
- Deletion
  - By Original Actor
    - Delete whole post
  - By Moderator Actor
    - Delete whole post


### Replies

- Text in reply to an Image post or Text Post
- Threaded
- Automatically Forwarded to all participating servers
  - Could get into trouble with current federation model
    - two non-federated servers could both have Actors reply to a mutually federated Post
    - Mutually-federated server could act as a proxy for account information (public keys, profile photo, etc)
- Creation
  - Inherits privacy from Parent post (not changeable)
    - Followers-only replies are upgraded to an unlisted-like privacy in order for disjoint followers circles to see all replies to a post
    - Post author account automatically Announces replies to followers
    - Replies are never independently visible (aside from to moderators)
  - Does not have 'sensitive' designation (rely on original post's designation)
  - Title is optional
  - Description is required
  - Can have attached mentions
    - inline
- Modification
  - By Original Reply's Actor
    - Change reply title
    - Change reply description
  - By Moderator Actor
    - Change reply title
    - Change reply description
    - Can Lock
      - Further replies to Locked replies will be rejected
- Report
  - By other User Actors
    - Report whole reply
- Deletion
  - By Original Post's Actor
    - Can Hide (reply title and description will be replaced with "Content Hidden by Page Owner")
      - Further replies to Hidden replies will be rejected
  - By Original Reply Actor
    - Can Hide (reply title and description will be replaced with "Content Hidden by Author")
      - Further replies to Hidden replies will be rejected
  - By Moderator Actor
    - Can Hide (reply title and description will be replaced with "Content Hidden by Moderator")
      - Further replies to Hidden replies will be rejected


### Folders

- Actor-level groupings of personal Image Posts
- Image posts can belong to one or more Folders
- All actors have a "default" Folder in which images are posted
- Creating or Modifying Folders does not cause notifications to any followers
- Folders are visible on the Actor's Profile page
- Creation
  - Can have different privacy levels
    - Unlisted (Visible to anyone)
    - Followers-Only (Visible to followers)
  - Title is required
  - Description is required
- Modification
  - By Original Actor
    - Change Folder Title
    - Change Folder Description
  - By Moderation Actor
    - Change Folder Title
    - Change Folder Description
    - Can Lock (prevent further edits)
- Deletion
  - By Original Actor
    - Can delete Folder
      - Does not delete images
      - Orphaned images are moved to the "default" Folder
  - By Moderation Actor
    - Can delete Folder
      - Does not delete images
      - Orphaned images are moved to the "default" Folder


### Collections

- Actor-level groupings of any Image Posts
- All actors have a default "Favorites" Collection
  - Favorites collection can be hidden
- Creating or Modifying Collections does not cause notifications to any followers
- Collections are visible on the Actor's Profile page
- Creation
  - Can have different privacy levels
    - Unlisted (Visible to anyone)
    - Followers-Only (Visible to followers)
    - None (Visible only to owning Actor)
  - Title is required
  - Description is required
- Modification
  - By Original Actor
    - Change Collection Title
    - Change Collection Description
    - Add/Remove Image Post
  - By Moderation Actor
    - Change Collection Title
    - Change Collection Description
    - Can Lock (prevent further edits)
- Deletion
  - By Original Actor
    - Can delete collection
  - By Moderation Actor
    - Can delete collection


### Feed Pages

- "Home" feed for followed Actor's image posts
- Separate "Local Posts" feed for viewing all public posts from the Actor's server
  - Contains a "Search" feature that allows searching by tag
  - Contains a "Sort by Hot/Trending" that ranks posts by favs/boosts
- Separate "All Posts" feed for viewing all public posts the Actor's server is federated with
  - Contains a "Search" feature that allows searching by tag
  - Contains a "Sort by Hot/Trending" that ranks posts by favs/boosts
- Separate "Journals" feed for viewing followed Actor's text posts
- Separate "Notifications" feed for Mentions, Replies, Favorites, Tag Requests, etc



## Additional pages

### Actor commissions info

- Commissions pages requirements
  - section for commission status
  - section for Terms of Service
  - Section for Prices and Examples
- Can likely be represented as an OrderedCollection with an Article for the Terms and Notes for each of the examples with prices
- Examples either a separate upload process from Image Posts, or existing image posts are used by reference
- Opening or Closing commissions should generate notifications for followers



## Stretch Goals

### Shared Collections

I'd like to be able to share Collections between multiple Actors. Maybe not even on the same server.
There will still be an "Owning" Actor on an "Owning" server, but the "Owning" actor can delegate
Add/Remove permissions to other actors.


### Groups

Similar to shared collections, but for creating posts. Artists will be able to upload images into a
Group, and all members of that Group will receive that image into their feeds. Groups can be used to
form community across server boundaries.
